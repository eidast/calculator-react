FROM node:14-alpine

WORKDIR /app

COPY package* ./
RUN npm install 

COPY . .
CMD [ "npm", "start" ]